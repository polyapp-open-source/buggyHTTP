package main

import (
	"github.com/rs/cors"
	"net/http"
	"os"
	"runtime"
	"time"
)

func main() {
	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("./serveThis"))
	mux.Handle("/", fs)
	handler := cors.New(cors.Options{
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT"},
		AllowedOrigins:   []string{"https://localhost:*", "http://localhost:*", "*"},
		AllowedHeaders:   []string{"Accept", "Accept-Language", "Accept-Encoding", "Content-Type", "Content-Length", "Authorization"},
		MaxAge:           120,
	}).Handler(mux)
	server := &http.Server{
		Addr:           ":5100",
		Handler:        handler,
		IdleTimeout:    60 * time.Second,
		ReadTimeout:    60 * time.Second,
		WriteTimeout:   60 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	var err error

	// working (the errors are expected - it succeeded because it says in the browser:
	// --- FAIL: TestAlwaysFail (0.00s)
	// main_integration_test.go:25: always fail
	tlsPath := ""
	if runtime.GOOS == "windows" {
		tlsPath = os.Getenv("TLSPATH") + "\\\\"
	} else {
		tlsPath = os.Getenv("TLSPATH") + string(os.PathSeparator)
	}
	err = server.ListenAndServeTLS(tlsPath+"server.crt", tlsPath+"server.key")

	// TODO not working
	// error in Firefox: DOMException: "The operation was aborted. "
	// error in Chrome: Failed to load resource: the server responded with a status of 404 (Not Found)
	// next error in Chrome: Failed to load resource: net::ERR_CONTENT_LENGTH_MISMATCH
	// next error in Chrome: TypeError: Could not download wasm module
	// err = server.ListenAndServe()

	if err != nil {
		panic(err)
	}
}
